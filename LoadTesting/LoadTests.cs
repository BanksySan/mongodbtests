﻿namespace LoadTesting
{
    using System;
    using System.Diagnostics;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using MongoDB.Bson;
    using MongoDB.Driver;
    using MongoDBTesting.Helpers;

    [TestClass]
    public class LoadTests
    {
        private static readonly RandomBsonDocumentGenerator DOCUMENT_GENERATOR = new RandomBsonDocumentGenerator();
        [TestMethod]
        public void FindByInteger()
        {
            var credential = MongoCredential.CreateCredential("test", "readonlyUser", "password");
            var settings = new MongoClientSettings
                           {
                               Credentials = new[] { credential },
                               Server = new MongoServerAddress("localhost")
                           };

            var mongoClient = new MongoClient(settings);

            var database = mongoClient.GetDatabase("test");

            var collection = database.GetCollection<BsonDocument>("oneMillionDocuments");

            var field = "adoptability-integer";

            var searchUpperValue = 1000000000;
            var filter = Builders<BsonDocument>.Filter.Lt(field, searchUpperValue);
            var count = 0;
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            using (var cursor = collection.FindAsync(filter).Result)
            {
                while (cursor.MoveNext())
                {
                    var batch = cursor.Current;

                    foreach (var document in batch)
                    {
                        Assert.IsTrue(document[field].AsInt32 < searchUpperValue);
                        count++;
                    }
                }
            }
            stopwatch.Stop();
            Console.WriteLine($"Found {count} documents in {stopwatch.ElapsedMilliseconds}ms.  Avg. {stopwatch.ElapsedMilliseconds / count}ms.");
        }

        [TestMethod]
        public void FindByStringContains()
        {
            var credential = MongoCredential.CreateCredential("test", "readonlyUser", "password");
            var settings = new MongoClientSettings
            {
                Credentials = new[] { credential },
                Server = new MongoServerAddress("localhost")
            };

            var mongoClient = new MongoClient(settings);

            var database = mongoClient.GetDatabase("test");

            var collection = database.GetCollection<BsonDocument>("oneMillionDocuments");

            const string FIELD = "tombless-long-string";

            const string SEARCH_WORD = "prodromal";
            var filter = Builders<BsonDocument>.Filter.Regex(FIELD, new BsonRegularExpression(SEARCH_WORD));

            var count = 0;
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            using (var cursor = collection.FindAsync(filter).Result)
            {
                while (cursor.MoveNext())
                {
                    var batch = cursor.Current;

                    foreach (var document in batch)
                    {
                        count++;
                        Assert.IsTrue(document[FIELD].AsString.Contains(SEARCH_WORD));
                    }
                }
            }

            stopwatch.Stop();
            Console.WriteLine($"Found {count} documents in {stopwatch.ElapsedMilliseconds}ms.  Avg. {stopwatch.ElapsedMilliseconds / count}ms.");
        }

        [TestMethod]
        public void InsertDocument()
        {
            var bson = DOCUMENT_GENERATOR.Generate();

            var credential = MongoCredential.CreateCredential("test", "readonlyUser", "password");
            var settings = new MongoClientSettings
            {
                Credentials = new[] { credential },
                Server = new MongoServerAddress("localhost")
            };

            var mongoClient = new MongoClient(settings);

            var database = mongoClient.GetDatabase("test");

            var collection = database.GetCollection<BsonDocument>("oneMillionDocuments");

            collection.InsertOne(bson);

            var documentId = bson["_id"];

            Assert.IsNotNull(documentId);

            var retrievedDocument = collection.Find(new BsonDocument("_id", new ObjectId(documentId.ToString()))).Single();

            Assert.IsNotNull(retrievedDocument);
        }
    }
}