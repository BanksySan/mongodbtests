﻿using NUnit.Framework;

namespace MongoDBTesting
{
    using System;
    using System.Linq;
    using Helpers;
    using MongoDB.Bson;
    using MongoDB.Driver;
    using NUnit.Framework.Compatibility;

    [TestFixture(Category = "Load Tests")]
    class BulkTests
    {
        [Test]
        public void InsertOneMillion()
        {
            var documentCount = 1000000;
            var documents = new DocumentEnumerator(documentCount);

            var credential = MongoCredential.CreateCredential("test", "writeUser", "password");
            var settings = new MongoClientSettings
                           {
                               Credentials = new[] { credential },
                               Server = new MongoServerAddress("localhost")
                           };

            var mongoClient = new MongoClient(settings);
            var database = mongoClient.GetDatabase("test");
            var collection = database.GetCollection<BsonDocument>($"loadTest-{DateTime.Now.ToString("u")}");

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            foreach (var document in documents)
            {
                collection.InsertOne(document);
            }
            
            stopwatch.Stop();

            Console.WriteLine($"Inserted {documentCount} documents.  Total time: {stopwatch.ElapsedMilliseconds}ms.  Avg. {stopwatch.ElapsedMilliseconds / documentCount}ms per document.");
        }
    }
}