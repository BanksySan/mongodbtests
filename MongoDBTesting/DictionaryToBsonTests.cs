﻿namespace MongoDBTesting
{
    using System;
    using System.Collections.Generic;
    using MongoDB.Bson;
    using NUnit.Framework;

    [TestFixture]
    internal class DictionaryToBsonTests
    {
        [Test]
        public void Convert()
        {
            var dictionary = new Dictionary<string, string>
            {
                {"field-1", "string-1"},
                {"field-2", "string-2"},
                {"field-3", "string-3"}
            };

            var bson = new BsonDocument
            {
                {"createdDate", new BsonDateTime(DateTime.Now)},
                {"content", new BsonDocument(dictionary)}
            };

            foreach (var item in dictionary)
            {
                Assert.That(bson["content"][item.Key].AsString, Is.EqualTo(dictionary[item.Key]));
            }
        }
    }
}