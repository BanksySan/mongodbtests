namespace MongoDBTesting.Helpers
{
    using System.Collections;
    using System.Collections.Generic;
    using MongoDB.Bson;

    internal class DocumentEnumerator : IEnumerator<BsonDocument>, IEnumerable<BsonDocument>
    {
        private readonly int _limit;
        private int _currentPosition;
        private readonly RandomWord _randomWord = new RandomWord();
        private static readonly RandomBsonDocumentGenerator DOCUMENT_GENERATOR = new RandomBsonDocumentGenerator();

        public DocumentEnumerator(int limit)
        {
            _limit = limit;
        }

        public void Dispose() { }

        public bool MoveNext()
        {
            if (_currentPosition == _limit)
            {
                return false;
            }

            _currentPosition++;

            return true;
        }

        public void Reset()
        {
            _currentPosition = 0;
        }

        public BsonDocument Current => DOCUMENT_GENERATOR.Generate();

        object IEnumerator.Current => Current;

        public IEnumerator<BsonDocument> GetEnumerator()
        {
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}