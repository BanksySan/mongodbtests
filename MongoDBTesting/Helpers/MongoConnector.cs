namespace MongoDBTesting.Helpers
{
    using MongoDB.Bson;
    using MongoDB.Driver;

    internal class MongoConnector
    {
        private const string DATABASE_NAME = "test";

        public static string ReadOnlyUser => "readonlyUser";

        public static string WriteUser => "writeUser";

        public IMongoCollection<BsonDocument> GetCollection(string user)
        {
            var credential = MongoCredential.CreateCredential(DATABASE_NAME, user, "password");
            var settings = new MongoClientSettings
            {
                Credentials = new[] {credential},
                Server = new MongoServerAddress("localhost")
            };

            var mongoClient = new MongoClient(settings);

            var database = mongoClient.GetDatabase(DATABASE_NAME);

            var collection = database.GetCollection<BsonDocument>("restaurants");
            return collection;
        }
    }
}