namespace MongoDBTesting.Helpers
{
    using System;
    using System.Text;
    using MongoDB.Bson;

    public class RandomBsonDocumentGenerator
    {
        private static readonly RandomWord RANDOM_WORD = new RandomWord();
        private static readonly Random RANDOM = new Random();

        public BsonDocument Generate()
        {
            var longTextBuilder = new StringBuilder();

            for (var i = 0; i < 1000; i++)
            {
                longTextBuilder.Append(RANDOM_WORD.GetWord()).Append(" ");
            }

            var doc = new BsonDocument
            {
                {"created", new BsonDateTime(DateTime.Now)},
                {RANDOM_WORD.GetWord(10) + "-integer", RANDOM.Next()},
                {RANDOM_WORD.GetWord(10) + "-double", RANDOM.NextDouble()},
                {RANDOM_WORD.GetWord(10) + "-short-string", RANDOM_WORD.GetWord(1000)},
                {RANDOM_WORD.GetWord(10) + "-long-string", longTextBuilder.ToString()}
            };

            return doc;
        }
    }
}