﻿namespace MongoDBTesting
{
    using System;
    using System.Collections.Concurrent;
    using System.Diagnostics;
    using System.Threading;
    using System.Threading.Tasks;
    using Helpers;
    using MongoDB.Bson;
    using MongoDB.Driver;
    using NUnit.Framework;

    [TestFixture]
    public class ParallelSearchingTests
    {
        private static readonly RandomWord RANDOM_WORD = new RandomWord();

        [Test]
        public void foo()
        {
            Console.WriteLine("Bar");

            Parallel.For(0,
                10,
                i =>
                {
                    Console.WriteLine(i + " started.");
                    Thread.Sleep(TimeSpan.FromSeconds(i));
                    Console.WriteLine(i + "ended.");
                });
        }

        [Test]
        public void ParallelSearch()
        {
            var iterations = 10;
            var results = new TestResultData[iterations];

            Parallel.For(0,
                iterations,
                i =>
                {
                    Console.WriteLine($"Starting test {i}");
                    var result = SearchTextFullText();
                    Console.WriteLine($"Finished test {i}");
                    results[i] = result;
                });

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
        }

        [Test]
        public void SearchTextRegex()
        {
            var credential = MongoCredential.CreateCredential("test", "readonlyUser", "password");
            var settings = new MongoClientSettings
                           {
                               Credentials = new[] { credential },
                               Server = new MongoServerAddress("localhost")
                           };

            var mongoClient = new MongoClient(settings);

            var database = mongoClient.GetDatabase("test");

            var collection = database.GetCollection<BsonDocument>("oneMillionDocuments");

            var searchWord = "supergalaxies";

            var filter = Builders<BsonDocument>.Filter.Regex("pistillate-long-string",
                new BsonRegularExpression("searchWord"));

            using (var cursor = collection.FindAsync(filter).Result)
            {
                while (cursor.MoveNext())
                {
                    var batch = cursor.Current;

                    Parallel.ForEach(batch, document => Assert.That(document, Is.Not.Null));
                }
            }
        }

        public TestResultData SearchTextFullText()
        {
            var credential = MongoCredential.CreateCredential("test", "readonlyUser", "password");
            var settings = new MongoClientSettings
                           {
                               Credentials = new[] { credential },
                               Server = new MongoServerAddress("localhost")
                           };

            var mongoClient = new MongoClient(settings);

            var database = mongoClient.GetDatabase("test");

            var collection = database.GetCollection<BsonDocument>("oneMillionDocumentsIndexed");

            var searchWord = RANDOM_WORD.GetWord();

            var filter = Builders<BsonDocument>.Filter.Text(searchWord);

            var stopwatch = new Stopwatch();

            stopwatch.Start();

            var docs = collection.Find(filter);

            stopwatch.Stop();

            var allDocs = docs.ToList();

            var count = allDocs.Count;
            var totalTime = stopwatch.ElapsedMilliseconds;

            return new TestResultData { DocumentCount = count, SearchWord = searchWord, TotalTime = totalTime };
        }

        public class TestResultData
        {
            public int DocumentCount { get; set; }
            public string SearchWord { get; set; }
            public long TotalTime { get; set; }

            public override string ToString()
            {
                return $"Search word: {SearchWord}\tDocument count: {DocumentCount}\tTotal time: {TotalTime}ms";
            }
        }

        [Test]
        public void QueryByDateAndText()
        {
            
        }
    }
}