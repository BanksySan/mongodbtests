﻿namespace MongoDBTesting
{
    using System;
    using Helpers;
    using MongoDB.Bson;
    using MongoDB.Driver;
    using NUnit.Framework;

    [TestFixture]
    internal class ReadOnlyUserAuthenticationTests
    {
        [Test]
        public void UserCannotInsert()
        {
            var collection = new MongoConnector().GetCollection(MongoConnector.ReadOnlyUser);
            var newDocument = new BsonDocument
            {
                {"value", "something"}
            };

            var e = Assert.Throws<MongoCommandException>(() => collection.InsertOne(newDocument));

            Assert.That(e, Is.Not.InstanceOf<TimeoutException>());
            Assert.That(e.Message, Does.StartWith("Command insert failed: not authorized"));
        }

        [Test]
        public void UserCanRead()
        {
            var collection = new MongoConnector().GetCollection(MongoConnector.ReadOnlyUser);

            var filter = Builders<BsonDocument>.Filter.Regex("cuisine", new BsonRegularExpression("Ice Cream"));

            var docs = collection.Find(filter).ToList();

            Assert.That(docs.Count, Is.GreaterThan(0));
        }
    }
}