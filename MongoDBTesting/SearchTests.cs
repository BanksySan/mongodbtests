﻿namespace MongoDBTesting
{
    using Helpers;
    using MongoDB.Bson;
    using MongoDB.Driver;
    using NUnit.Framework;

    [TestFixture]
    internal class SearchTests
    {
        [Test]
        public void AndSearch()
        {
            var builder = Builders<BsonDocument>.Filter;

            var filter = builder.Eq("cuisine", "Italian") & builder.Eq("address.zipcode", "10075");

            var collection = new MongoConnector().GetCollection(MongoConnector.ReadOnlyUser);

            var result = collection.Find(filter).ToList();

            Assert.That(result.Count, Is.GreaterThan(0));
        }

        [Test]
        public void GradeCountGreaterThanTwo()
        {
            var filter = Builders<BsonDocument>.Filter.SizeLt("grades", 2);

            var collection = new MongoConnector().GetCollection(MongoConnector.ReadOnlyUser);

            var result = collection.Find(filter).ToList();

            Assert.That(result.Count, Is.GreaterThan(0));
        }
    }
}