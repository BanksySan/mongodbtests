﻿namespace MongoDBTesting
{
    using System;
    using Helpers;
    using MongoDB.Bson;
    using MongoDB.Driver;
    using NUnit.Framework;

    [TestFixture]
    internal class WriteUserTests
    {
        private static readonly Random RANDOM = new Random();

        [Test]
        public void UserCanWrite()
        {
            var bson = new BsonDocument
            {
                {"created", new BsonDateTime(DateTime.Now)},
                {"field-1", "value-1"},
                {
                    "field-2", new BsonDocument
                    {
                        {"Random", RANDOM.Next()}
                    }
                }
            };

            var collection = new MongoConnector().GetCollection(MongoConnector.WriteUser);

            collection.InsertOne(bson);

            var documentId = bson["_id"];

            Assert.That(documentId, Is.Not.Null);

            var retrievedDocument = collection.Find(new BsonDocument("_id", new ObjectId(documentId.ToString()))).Single();

            Assert.That(retrievedDocument, Is.Not.Null);
        }
    }
}